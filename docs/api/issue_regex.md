# Issue Regex

## Get

Get a single IssueRegex.

`GET /api/1/issue/-/regex/$issue_regex_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `issue_regex_id` | `int` | Yes | ID of the IssueRegex. |

Example of response:

```json
{
    "id": 886,
    "issue": {
        "id": 935,
        "kind": {
            "id": 4,
            "description": "Unidentified",
            "tag": "Unidentified"
        },
        "description": "Issue description",
        "ticket_url": "https://ticket_url",
        "resolved": false,
        "resolved_at": null,
        "policy": {
            "id": 1,
            "name": "public"
        },
        "first_seen": "2022-01-19T15:28:30.866568Z"
    },
    "text_match": "text match",
    "file_name_match": "file name match",
    "test_name_match": "test name match",
    "architecture_match": "architecture match",
    "tree_match": "tree match",
    "kpet_tree_name_match": "kpet tree name match",
    "package_name_match": "package name match"
}
```

## List

Get a list of IssuesRegexes.

`GET /api/1/issue/-/regex`

Example of response:

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 886,
            "issue": {
                "id": 935,
                "kind": {
                    "id": 4,
                    "description": "Unidentified",
                    "tag": "Unidentified"
                },
                "description": "Issue description",
                "ticket_url": "https://ticket_url",
                "resolved": false,
                "resolved_at": null,
                "policy": {
                    "id": 1,
                    "name": "public"
                },
                "first_seen": "2022-01-19T15:28:30.866568Z"
            },
            "text_match": "text match",
            "file_name_match": "file name match",
            "test_name_match": "test name match",
            "architecture_match": "architecture match",
            "tree_match": "tree match",
            "kpet_tree_name_match": "kpet tree name match",
            "package_name_match": "package name match"
        }
    ]
}
```
