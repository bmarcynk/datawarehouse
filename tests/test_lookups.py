"""Test the custom lookups."""
from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import models
from tests import utils


class TestIsNotNull(utils.TestCase):
    """Test custom lookup "isnotnull"."""
    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
    ]

    def test_preconditions(self):
        """Ensure required conditions are OK for the tests."""
        self.assertTrue(models.Issue.objects.exists())

    @freeze_time("2010-01-02 09:00:00")
    def test_resolved_not_resolved(self):
        """Test update_issues correctly updates cki_issues_resolved."""
        total_count = models.Issue.objects.count()

        with self.subTest(resolved_at=None):
            models.Issue.objects.update(resolved_at=None)

            self.assertEqual(
                models.Issue.objects.filter(resolved_at__isnotnull=False).count(),
                total_count,
            )
            self.assertEqual(
                models.Issue.objects.filter(resolved_at__isnotnull=True).count(),
                0,
            )

        with self.subTest(resolved_at=timezone.now()):
            models.Issue.objects.update(resolved_at=timezone.now())

            self.assertEqual(
                models.Issue.objects.filter(resolved_at__isnotnull=True).count(),
                total_count,
            )
            self.assertEqual(
                models.Issue.objects.filter(resolved_at__isnotnull=False).count(),
                0,
            )
