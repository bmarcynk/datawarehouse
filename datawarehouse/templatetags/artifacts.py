"""Artifacts templatetags."""
import datetime

from django import template
from django.conf import settings
from django.utils import timezone

register = template.Library()


def older_than_artifacts_default_expiry(timestamp):
    """Return True if the timestamp is older default artifacts expiry."""
    if not timestamp:
        return False

    oldest_available = (
        timezone.now() -
        datetime.timedelta(days=settings.ARTIFACTS_DEFAULT_VALID_FOR_DAYS)
    )

    return timestamp < oldest_available


register.filter(older_than_artifacts_default_expiry)
