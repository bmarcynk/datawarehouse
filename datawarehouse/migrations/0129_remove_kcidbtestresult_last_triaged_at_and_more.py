# Generated by Django 4.1.13 on 2024-01-16 19:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0128_remove_kcidbbuild_kpet_tree_family'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='kcidbtestresult',
            name='last_triaged_at',
        ),
        migrations.AlterField(
            model_name='issueoccurrence',
            name='related_checkout',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_issue_occurrences', to='datawarehouse.kcidbcheckout'),
        ),
    ]
