# Generated by Django 3.2.4 on 2021-06-21 08:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0043_artifact_url_remove_duplicates'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artifact',
            name='url',
            field=models.URLField(max_length=400, unique=True),
        ),
    ]
