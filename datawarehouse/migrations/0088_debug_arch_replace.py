"""Replace x86_64_debug arch with x86_64 + debug."""
from django.db import migrations


def remove_x86_64_debug(apps, schema_editor):
    """Replace x86_64_debug arch."""
    KCIDBBuild = apps.get_model('datawarehouse', 'KCIDBBuild')
    db_alias = schema_editor.connection.alias

    KCIDBBuild.objects.using(db_alias).filter(
        architecture=7,  # ArchitectureEnum.x86_64_debug
    ).update(
        architecture=6,  # ArchitectureEnum.x86_64
        debug=True
    )


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0087_add_flag_for_umb_messages'),
    ]

    operations = [
        migrations.RunPython(remove_x86_64_debug),
    ]
