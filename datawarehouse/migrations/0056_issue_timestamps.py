# Generated by Django 3.2.6 on 2021-08-16 12:01

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('datawarehouse', '0055_issues_users'),
    ]

    operations = [
        migrations.AddField(
            model_name='issue',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='issue',
            name='last_edited_on',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='issueregex',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='issueregex',
            name='last_edited_on',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='issue',
            name='created_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='issue_created', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='issue',
            name='last_edited_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='issue_edited', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='issueregex',
            name='created_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='issueregex_created', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='issueregex',
            name='last_edited_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='issueregex_edited', to=settings.AUTH_USER_MODEL),
        ),
    ]
