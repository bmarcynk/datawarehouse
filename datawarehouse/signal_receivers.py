"""Receiver functions for signals."""
from cki_lib.logger import get_logger
from django import dispatch
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.signals import m2m_changed
from django.db.models.signals import post_save
from django.db.models.signals import pre_save

from datawarehouse import models
from datawarehouse import scripts
from datawarehouse import signals
from datawarehouse import utils
from datawarehouse.api.kcidb import serializers as kcidb_serializers

User = get_user_model()

LOGGER = get_logger(__name__)


@dispatch.receiver(signals.kcidb_object)
def send_kcidb_object_message(status, object_type, objects, misc=None, **_):
    """Send new kcidb object message."""
    # NOTE: prefetching related models is advised to reduce extra queries while serializing
    # serialize here and not in message_rabbitmq.py to avoid circular dependency on models
    serializers = {
        'checkout': kcidb_serializers.KCIDBCheckoutSerializer,
        'build': kcidb_serializers.KCIDBBuildSerializer,
        'test': kcidb_serializers.KCIDBTestSerializer,
        'testresult': kcidb_serializers.KCIDBTestResultSerializer,
    }
    LOGGER.info("Serializing %s to send as a message.", object_type)
    content = [serializers[object_type](o).data for o in objects]
    LOGGER.info("Serialized %d %s to send as a message.", len(content), object_type)
    utils.MSG_QUEUE.bulk_add(status, object_type, content, misc or {})


@dispatch.receiver(post_save, sender=models.IssueRegex)
def issue_regex_modified(instance, **_):
    """Send kcidb objects for retriage after regexes are added / modified."""
    models.QueuedTask.create(
        name='datawarehouse.scripts.misc.send_kcidb_object_for_retriage',
        call_id='send_kcidb_object_for_retriage',
        call_kwargs=[{
            'issueregex_id': instance.id,
        }],
        run_in_minutes=settings.TIMER_RETRIAGE_PERIOD_S / 60
    )


@dispatch.receiver(m2m_changed, sender=models.KCIDBCheckout.issues.through)
@dispatch.receiver(m2m_changed, sender=models.KCIDBBuild.issues.through)
@dispatch.receiver(m2m_changed, sender=models.KCIDBTest.issues.through)
def issue_occurrence_assigned(action, sender, instance, pk_set, **_):
    """Signal handler to populate extra fields in IssueOccurrence and propagate notifications."""
    if sender != models.IssueOccurrence:
        raise NotImplementedError

    if action == 'post_add':
        scripts.update_issue_occurrences_related_checkout(instance, pk_set)
        scripts.update_issue_policy(pk_set)
        scripts.update_issue_occurrences_regression(instance, pk_set)
        scripts.send_regression_notification(instance, pk_set)

    if action in {"post_add", "pre_remove", "pre_clear"}:
        # select issue ids manually, as pre_clear doesn't provide them
        issues_ids = instance.issues.values_list("id", flat=True) if action == "pre_clear" else pk_set
        scripts.notify_issueoccurrences_changed(instance, issues_ids, action)


@dispatch.receiver(post_save, sender=User)
def sync_user_ldap_groups(created, instance, **_):
    """Trigger LDAP groups sync after user creation."""
    if created:
        scripts.update_ldap_group_members_for_user(instance)


@dispatch.receiver(pre_save, sender=models.KCIDBCheckout)
@dispatch.receiver(pre_save, sender=models.KCIDBBuild)
@dispatch.receiver(pre_save, sender=models.KCIDBTest)
def clear_issues_occurrences_on_save(sender, instance, **_):
    """Clear the issue occurrences from the updated kcidb object if it's status changed."""
    if not settings.CLEAR_ISSUEOCCURRENCES_ON_SAVE:
        return

    # If the kcidb object is not being created, and changed enough to not be equivalent,
    # it's likely that the pipeline job got retried creating a brand new set of logs.
    # Either it shouldn't have issues anymore (pass|skip|miss) or get triaged anew
    if (
        (old_instance := sender.objects.filter(id=instance.id).first()) is not None
        and not old_instance.is_test_plan
        and not old_instance.is_equivalent_to(instance)
    ):
        LOGGER.warning(
            "Clearing issue occurrences from %r because it's changed considerably",
            instance,
        )
        instance.issues.clear()
