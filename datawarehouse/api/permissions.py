"""Permissions module."""
from cki_lib.logger import get_logger
from rest_framework import filters
from rest_framework import permissions

from datawarehouse.authorization import PolicyAuthorizationBackend

PERMISSION_METHOD = permissions.DjangoModelPermissions()
LOGGER = get_logger(__name__)


class DjangoModelPermissionOrReadOnly(permissions.BasePermission):
    """DjangoModelPermissions or ReadOnly."""

    def has_permission(self, request, view):
        """Define if the user has permission for the request."""
        request_is_safe = request.method in permissions.SAFE_METHODS
        user_has_permissions = PERMISSION_METHOD.has_permission(request, view)
        LOGGER.debug("[%r]PERM model check for %s", request, view)

        has_permission = request_is_safe or user_has_permissions
        logger_perm = "has permission (allow)" if has_permission else "has no permission (deny)"
        LOGGER.debug("[%r]PERM user %s", request, logger_perm)
        return request_is_safe or user_has_permissions


class PolicyAuthorizationPermission(permissions.BasePermission):
    """PolicyAuthorizationPermission based on Policy."""

    def has_object_permission(self, request, view, obj):
        """Define if the user has permission for the object in a request."""
        method = "read" if request.method in permissions.SAFE_METHODS else "write"
        path_to_policy = PolicyAuthorizationBackend.get_path_to_policy(obj)
        user_has_permissions = PERMISSION_METHOD.has_permission(request, view)
        LOGGER.debug("[%r]PERM %s check for object %s in %s", request, method, obj, view)

        policy = getattr(obj, path_to_policy)
        if policy is None:
            # Is private allow nobody
            LOGGER.debug("[%r]PERM %s is private (deny)", request, obj)
            return False

        group = getattr(policy, f"{method}_group")
        if group is None:
            # Is public allow anybody to edit, creation only on model permissions
            LOGGER.debug("[%r]PERM %s is public (allow)", request, obj)
            return True

        has_permission = group.id in PolicyAuthorizationBackend.get_user_groups(request) or user_has_permissions
        logger_perm = "has permission (allow)" if has_permission else "has no permission (deny)"
        LOGGER.debug("[%r]PERM user %s for %s", request, logger_perm, obj)
        return has_permission


class PolicyFilterBackend(filters.BaseFilterBackend):
    """Filter that only allows users to see only objects they are authorized to view."""

    def filter_queryset(self, request, queryset, view):
        """Return queryset the user is authorized to view."""
        return PolicyAuthorizationBackend.filter_authorized(request, queryset)
